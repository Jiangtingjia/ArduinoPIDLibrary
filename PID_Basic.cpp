/********************************************************
* PID Basic Example
* Reading analog input 0 to control analog PWM output 3
********************************************************/

#include "PID_v1.h"

// 分别表示控制量及被控量的IO口：PIN_INPUT 对应被控量 PIN_OUTPUT 对应控制量
#define PIN_INPUT 0
#define PIN_OUTPUT 3

//Define Variables we'll be connecting to
// 定义设定值、 被控量、 控制量
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
//初始PID控制参数
double Kp = 2, Ki = 5, Kd = 1;
// 构造PID控制器
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// 赋值PID控制器基本变量
void setup()
{
	//initialize the variables we're linked to
	Input = analogRead(PIN_INPUT);
	Setpoint = 100;

	//turn the PID on
	myPID.SetMode(AUTOMATIC);
}

// 循环调用PID进行调节
void loop()
{
	Input = analogRead(PIN_INPUT);
	myPID.Compute();
	analogWrite(PIN_OUTPUT, Output);
}


