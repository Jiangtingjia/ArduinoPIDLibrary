
#include "PID_AutoTune_v0.h"

// 构造函数
PID_ATune::PID_ATune(double* Input, double* Output)
{
	// 赋值输入、输出
	input = Input;
	output = Output;
	// 默认控制模式为PI
	controlType =0 ; //default to PI  默认为PI控制
	// 默认噪声带宽为0.5
	noiseBand = 0.5;	//噪声带宽为0.5，具体0.5代表什么还未知
	// 默认运行状态为false
	running = false;	//
	// 默认output PWM半峰值为30
	oStep = 30;			
	// 默认设置回溯为10个点
	SetLookbackSec(10);
	// 此处需要改写为当前运行时间
	lastTime = millis();
}


// 此函数用于取消PID 控制
void PID_ATune::Cancel()
{
	running = false;
} 
 
//计算运行时间
int PID_ATune::Runtime()
{
	// 开始计算标志位
	justevaled=false;
	/***********************************************/
	// 如果峰值数量大于9，并且运行标志位置1，那么置运行标志位为结束，并且条用finishup函数，并且返回函数退出，计算结束（非零就代表结束）
	// 这里为何峰谷数量大于9个、并且运行标志位 = 1就表示可以计算？？？？？？？？？？？？？？？？？
	if(peakCount > 9 && running)
	{
		// 设置运行标志位 = 0 ；
		running = false;
		// 这个函数是用来计算PID参数调谐因子的
		FinishUp();
		return 1;
	}
	/***********************************************/

	// 获取当前时间，此时间主要是用来判断时间间隔是否超过采样间隔
	unsigned long now = millis();
	/***********************************************/
	//如果当前运行时间与初始化的时间差小于采样时间，那么函数退出，计算失败，也就是意味着，未满足采样间隔，不计算
	if((now-lastTime)<sampleTime) return false;
	/***********************************************/

	// 如果超过了采样时间，那么需要进行计算
	// 更新这次计算对应的运行时间，以便下一次计算是否满足采样间隔需求
	lastTime = now;
	// 参考值赋值为input，此input对应的刚好是采样间隔点的控制量
	double refVal = *input;
	// 可计算标志位 = 1；
	justevaled=true;
	// 如果非运行（running = 0），初始化所有变量，如果已经开始运行了
	if(!running)
	{ //initialize working variables the first time around
		// 峰值模式 = 0 。既不是峰顶 也不是峰谷
		peakType = 0;
		// 峰值数量
		peakCount=0;
		// 触发改变设置为0
		justchanged=false;
		// 最大值绝对为采样间隔点对应的控制量
		absMax=refVal;
		// 最小值绝对为采样间隔点对应的控制量
		absMin=refVal;
		// 设定值为采样间隔点对应的控制量，这里有设么意义，仅在稳态的时候才需要调谐？
		setpoint = refVal;
		// 运行设置为真
		running = true;
		// 输出开始为当前ouput
		outputStart = *output;
		// 当前输出 = 当前输出+ oStep（固定值为30），这里30可能不行，温度可能上不去
		*output = outputStart+oStep;
	}
	else
		// 在采样过程中，不断的找到最大最小值
	{
		// 如果参考值 比 最大值的绝对值要大， 那么设置 最大值绝对值为参考值
		if(refVal>absMax)absMax=refVal;
		// 如果参考值 比 最小值的绝对值要大， 那么设置 最小值绝对值为参考值
		if(refVal<absMin)absMin=refVal;
	}
	
	// 总结：控制量超过触发线，输出反向，以便输出和控制量永远处于反向状态，这样子，可以维持控制在在setpoint附近，也就是initial时对应的控制量
	//oscillate the output base on the input's relation to the setpoint
	// 如果参考值 比 设定值+噪声贷款要大，设置output = outputStart - oStep;
	if(refVal>setpoint+noiseBand) *output = outputStart-oStep;
	// 如果参考值 比 设定值-噪声带宽要小，设置output = outputStart + oStep;
	else if (refVal<setpoint-noiseBand) *output = outputStart+oStep;
	
	
  //bool isMax=true, isMin=true;暂时设置最大最小值标志位为真，后续会根据条件更改
  isMax=true;isMin=true;
  //id peaks，回溯数量，按照先进先出的原则将控制量存储在lastInputs中，最新的值存在0，依次类推
  for(int i=nLookBack-1;i>=0;i--)
  {
	  // 判断是否峰值
    double val = lastInputs[i];
	//如果 参考值前面所有点大，那么是可能的最大值
    if(isMax) isMax = refVal>val;
	//如果 参考值比前面所有的点小，那么是可能的最小值
    if(isMin) isMin = refVal<val;
	// 更新队列，先进先出
    lastInputs[i+1] = lastInputs[i];
  }
  lastInputs[0] = refVal; 

  //如果nLookBack小于9，那么计算失败，退出，也就是意味着，窗口必须比9个点大。
  if(nLookBack<9)
  {  
	  //we don't want to trust the maxes or mins until the inputs array has been filled
	  // 如果储存的控制量的值数量小于9个，那么，计算结果将不可信，这个地方可以稍作修改
	return 0;
  }
  
  // 如果是可能的最大或最小值
  // 这里必然会先找到一个极值
  if(isMax)
  {
	  // 如果峰值标志位0，那么置1，即可能是峰顶
    if(peakType==0)peakType=1;
	// 如果是-1，那么置1，设置转换标志位真，peak2 = peak1
    if(peakType==-1)
    {
      peakType = 1;
	  // 状态转换
      justchanged=true;
	  // 记录峰值时间
      peak2 = peak1;
    }
	// peak1 设置为当前运行时间
    peak1 = now;
	//  可能的峰值 = 当前控制量
    peaks[peakCount] = refVal;
   
  }
  //如果是最小值
  else if(isMin)
  {
	  // 如果峰值标志位0，那么置-1
    if(peakType==0)peakType=-1;
	// 如果是1，那么置-1，设置转换标志位真，peakcount ++，这个量只有到了峰谷才会增加，但无论峰顶还是峰谷都会更新为可能的极值
    if(peakType==1)
    {
      peakType=-1;
      peakCount++;
      justchanged=true;
    }
    // 如果peakCount 小于10， 那么峰值数组设置为参考值
    if(peakCount<10) peaks[peakCount] = refVal;
  }
  
  // 如果转换标志置1 并且峰值数量 >2 
  if(justchanged && peakCount > 2)
  { //we've transitioned.  check if we can autotune based on the last peaks
	// avg = |p[i-1] - p[i-2]| + |p[i-2] - p[i-3]|		/2
    double avgSeparation = (abs(peaks[peakCount-1]-peaks[peakCount-2])+abs(peaks[peakCount-2]-peaks[peakCount-3]))/2;
	// 如果平均avg 在峰峰值5%内波动，执行finishup 并结束运行， 返回成功标志位，峰顶稳定了才开始调节
    if( avgSeparation < 0.05*(absMax-absMin))
    {
	  FinishUp();
      running = false;
	  return 1;
	 
    }
  }
  // 如果没有设置 转换为0 ， 失败退出
   justchanged=false;
	return 0;
}

// 官方未注释
void PID_ATune::FinishUp()
{
	  *output = outputStart;
      //we can generate tuning parameters!
	  // 计算Ku  Pu
      Ku = 4*(2*oStep)/((absMax-absMin)*3.14159);
	  // 这里为什么需要/1000 ，不清楚
      Pu = (double)(peak1-peak2) / 1000;
}


// 此处三个函数用于更改PID参数
/****************************/
double PID_ATune::GetKp()
{
	return controlType==1 ? 0.6 * Ku : 0.4 * Ku;
}

double PID_ATune::GetKi()
{
	return controlType==1? 1.2*Ku / Pu : 0.48 * Ku / Pu;  // Ki = Kc/Ti
}

double PID_ATune::GetKd()
{
	return controlType==1? 0.075 * Ku * Pu : 0;  //Kd = Kc * Td
}
/****************************/


// 此处用于设置输出output 的半峰值
void PID_ATune::SetOutputStep(double Step)
{
	oStep = Step;
}


// 用于获取output半峰值
double PID_ATune::GetOutputStep()
{
	return oStep;
}

// 设置控制模式 0 代表PI  1代表 PID
void PID_ATune::SetControlType(int Type) //0=PI, 1=PID
{
	controlType = Type;
}

//获取当前的控制模式  PID还是PID
int PID_ATune::GetControlType()
{
	return controlType;
}

// 设置噪声带宽
void PID_ATune::SetNoiseBand(double Band)
{
	noiseBand = Band;
}

//获取噪声带宽
double PID_ATune::GetNoiseBand()
{
	return noiseBand;
}

//获取lookback时间，最小为1. 小于25，设定采样值为250，lookback数量为100，否则，数量为100，采样周期在value基础上放大10倍
void PID_ATune::SetLookbackSec(int value)
{
    if (value<1) value = 1;
	
	if(value<25)
	{
		nLookBack = value * 4;
		sampleTime = 250;
	}
	else
	{
		nLookBack = 100;
		sampleTime = value*10;
	}
}

//获取lookback时间，转换为s
int PID_ATune::GetLookbackSec()
{
	return nLookBack * sampleTime / 1000;
}
